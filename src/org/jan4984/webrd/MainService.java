package org.jan4984.webrd;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

public class MainService extends Service {

	static final String TAG = "MainService";
	SimpleWebServer mWebServer = null;
	public static File WORKING_DIR = null;

	@Override
	public void onCreate(){
		try{
			File currentDir = this.getFilesDir();
			mWebServer = new SimpleWebServer("0.0.0.0", 2048, currentDir, false);
			mWebServer.setContext(this);
			copy(R.raw.index, new File(currentDir, "index.html"));
			copy(R.raw.client, new File(currentDir, "client.jar"));
			WORKING_DIR = currentDir;
		}catch(Exception e){
			Log.e(TAG, "oncreate", e);
		}
	}
	
	@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
		try {
			InputInjector.startServerProcess();
			DisplayMetrics displaymetrics = new DisplayMetrics();
			WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
			wm.getDefaultDisplay(). getRealMetrics(displaymetrics);
			int height = displaymetrics.heightPixels;
			int width = displaymetrics.widthPixels;
			FrameBuffer.connect(width, height, this.getFilesDir());
			mWebServer.start();
		} catch (IOException e) {
			Log.e(TAG, "starting web server", e);
		}
        return START_STICKY;
    }
	
	@Override
	public void onDestroy(){
		mWebServer.stop();
		InputInjector.disconnect();		
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	private void copy(int id, File fo){
		try{
			Log.d(TAG, "cp " + id + " to " + fo.getAbsolutePath());
			InputStream is = getResources().openRawResource(id);
			OutputStream os = new FileOutputStream(fo);
			int rd;
			do{
				rd = is.read();
				if(rd == -1) break;
				os.write(rd);
			}while(true);
			is.close();
			os.close();
		}catch(Exception e){
			Log.e(TAG, "copy " + id + " to " + fo.getAbsolutePath(), e);		
		}
	}
	
	public static void startServer(Context cxt){
		Intent i = new Intent();
		i.setClassName(cxt, "org.jan4984.webrd.MainService");
		cxt.startService(i);
	}

	public static int runInSuClasspath(String cmd, String cp, String[] envp) throws IOException, InterruptedException{
		Log.d(TAG, "executing su with cp and envp " + cmd);
		Log.d(TAG, "cp:" + cp);
		for(String s : envp){
			Log.d(TAG, s);
		}
		Process p = Runtime.getRuntime().exec("CLASSPATH=" + cp + " /system/xbin/su -- root " + cmd,
				envp,
				WORKING_DIR);
		int ret = p.waitFor();
		Log.d(TAG, "returned " + ret);
		return ret;
	}
	public static int runInSu(String cmd, String[] envp, boolean sync) throws IOException, InterruptedException{
		Log.d(TAG, "executing su with envp " + cmd);
		for(String s : envp){
			Log.d(TAG, s);
		}
		Process p = Runtime.getRuntime().exec("/system/xbin/su -- root " + cmd,
				envp,
				WORKING_DIR);
		if(sync){
			int ret = p.waitFor();
			Log.d(TAG, "returned " + ret);
			return ret;
		}
		return 0;
	}
	public static int runInSu(String cmd) throws IOException, InterruptedException{		
		Log.d(TAG, "executing su " + cmd);
		
		Process p = Runtime.getRuntime().exec("/system/xbin/su -- root " + cmd,
				null,
				WORKING_DIR);
		int ret = p.waitFor();
		Log.d(TAG, "returned " + ret);
		return ret;
	}
}
