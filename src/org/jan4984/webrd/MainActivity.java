package org.jan4984.webrd;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.hardware.input.InputManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	static final String TAG = "MainActivity"; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button btn = (Button)this.findViewById(R.id.button1);
		btn.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				MainService.startServer(MainActivity.this);
			}
		});

		btn = (Button)this.findViewById(R.id.button3);
		btn.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				try {
					InputInjector.touchDown(10, 10);
					InputInjector.touchUp(20, 20);
				} catch (Exception e) {
					Log.d(TAG, "inject event", e);
				}				
			}
		});
		
	}
	public boolean onKeyUp(int keycode, KeyEvent evt){
		Log.d(TAG, "onkey:" + keycode);
		return super.onKeyUp(keycode, evt);
	}

	@Override
	public boolean onTouchEvent (MotionEvent event){
		Log.d(TAG, "touch event:" + event.getButtonState() + ", x:" + event.getX() + ", y:" + event.getY());
		return super.onTouchEvent(event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onDestroy(){
		super.onDestroy();
	}
}
