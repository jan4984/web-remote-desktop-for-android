package org.jan4984.webrd;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.util.Log;

public class FrameBuffer {
	static final String TAG = "FrameBuffer";
	static Bitmap bmp = null;
	static File workingDir;

	public static InputStream obtainNewFrameBuffer(int width, int height, int quality){
		ByteArrayInputStream dummy = new ByteArrayInputStream(new byte[0]);
		try{

			ByteArrayOutputStream out = new  ByteArrayOutputStream();
			if(getNewscreencap() == 0){											
				if(width < 0) width = bmp.getWidth();
				if(height < 0) height = bmp.getHeight();
				File src = new File(workingDir, "screen.png");
				if(width != bmp.getWidth() || height != bmp.getHeight()){
					Bitmap orgBmp = BitmapFactory.decodeFile(src.getAbsolutePath());
					Bitmap bmpScaled = Bitmap.createScaledBitmap(orgBmp, width, height, true);
					orgBmp.recycle();
					bmpScaled.compress(CompressFormat.PNG, quality, out);
					bmpScaled.recycle();
				}else{
					return new FileInputStream(src);
				}
			}
			return new ByteArrayInputStream(out.toByteArray());
		}catch(Exception e){
			Log.e(TAG, "obtaining frame buffer image", e);
		}
		return dummy;
	}
		
	public static void connect(int w, int h, File workingDir){
		try {
			Log.d(TAG, String.format("width:%d, height:%d, workingDir:%s", w, h, workingDir.getAbsoluteFile()));
			bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
			bmp.setHasAlpha(false);
			FrameBuffer.workingDir = workingDir;		
		} catch (Exception e) {
			Log.e(TAG, "connecting native server", e);
		}
	}
	
	public static void disconnect(){		
		bmp.recycle();
		bmp = null;		
	}

	private static int getNewscreencap(){
		try {
			int ret = MainService.runInSu("/system/bin/screencap -p screen.png");
			ret += MainService.runInSu("chmod 777 screen.png");
			return ret;
		} catch (Exception e) {
			Log.e(TAG, "executing screencap", e);
		}
		return 1;
	}
}
