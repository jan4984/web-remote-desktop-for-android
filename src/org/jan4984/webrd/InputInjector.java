package org.jan4984.webrd;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Map;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.util.Log;

public class InputInjector {
	private static InputInjector THIS;
	final static String TAG = "InputInjector";
	private static OutputStream OS=null;
	private static Socket LS = null;
	private static volatile boolean SREVER_STARTED = false;

	public static synchronized void startServerProcess(){
		if(SREVER_STARTED){
			return;
		}
		try {
			LinkedList<String> envps = new LinkedList<String>();
			envps.add("CLASSPATH=" 
					+ MainService.WORKING_DIR.getAbsolutePath() 
					+ "/client.jar");
			envps.add("BOOTCLASSPATH=" + System.getenv("BOOTCLASSPATH"));
			envps.add("ANDROID_STORAGE=" + System.getenv("ANDROID_STORAGE"));
			envps.add("EMULATED_STORAGE_SOURCE=" + System.getenv("EMULATED_STORAGE_SOURCE"));
			envps.add("EXTERNAL_STORAGE=" + System.getenv("EXTERNAL_STORAGE"));
			envps.add("PATH=" + System.getenv("PATH"));
			 //Map<String, String> env = System.getenv();
		     //   for (String envName : env.keySet()) {
		     //       envps.add(String.format("%s=%s%n",
		     //                         envName,
		     //                         env.get(envName)));
		     //   }
			MainService.runInSu("/system/bin/app_process /system/bin/ " 					
					+ "org.jan4984.client.InjectorServer",
					envps.toArray(new String[]{}),
					false);
		} catch (Exception e) {
			Log.e(TAG, "starting server process", e);
			return;
		}
		SREVER_STARTED = true;
	}

	private static synchronized void connect(){		
		try {
			LS = new Socket("127.0.0.1", 4567);
			OS = LS.getOutputStream();
			Log.d(TAG, "connected");			
		} catch (Exception e) {
			Log.e(TAG, "connecting to server", e);
		}
	}
	public static void disconnect(){
		try{
			LS.close();
		}catch(Exception e){}
	}
	public static void keyPress(int code){
		sendCmd(" key", code);
	}
	public static void touchDown(int x, int y){
		sendCmd(" down", x, y);
	}

	public static void touchDrag(int x, int y){
		sendCmd(" move", x, y);
	}
	
	public static void touchUp(int x, int y){
		sendCmd(" up", x, y);
	}
	private static void sendCmd(String act, int p){
		String cmd = act + " " + p;
		sendRawMessage(cmd.getBytes());
		Log.d(TAG, "sent " + cmd);
	}
	private static void sendCmd(String act, int x, int y){
		String cmd = act + " " + x + " " + y;
		sendRawMessage(cmd.getBytes());
		Log.d(TAG, "sent " + cmd);
	}
	private static synchronized void sendRawMessage(byte[] data){
		while(OS == null){
			connect();
		}
		try{
			OS.write(data);
		}catch(Exception e){
			Log.e(TAG, "sending command", e);
			OS = null;			
		}
	}
	
/*	int m_fd;
	final static int EV_KEY = 0x01;
	final static int EV_REL = 0x02;
	final static int EV_ABS= 0x03;
	final static int ABS_X= 0x0;
	final static int ABS_Y= 0x01;
	static String EVT_FILE  = null;	
	final static int KEY_REL_X = 0x0;
	final static int KEY_REL_Y = 0x1;
	final static int BTN_TOUCH = 0x14a;
	public final static int KEY_CODE_A = 30;
	
	public static final int BTN_LEFT = 0x110;
	public static final int BTN_RIGHT = 0x111;
		
	public synchronized static InputInjector get(int inputId){		
		if(THIS == null){
			EVT_FILE = "/dev/input/event" + inputId;
			THIS = new InputInjector();			
		}
		return THIS;
	}
	public synchronized static InputInjector get(){
		return THIS;
	}

	private InputInjector() {
		prepareEventFile();
		intEnableDebug(1);		
		//m_fd = intCreate(EVT_FILE, 1, 0);
		m_fd = intOpen(EVT_FILE);
	}
	
	private synchronized void prepareEventFile(){
		try {
			//File f = new File(EVT_FILE);
			//MainService.runInSu("echo 1 > " + EVT_FILE);
			//Thread.sleep(20);
			MainService.runInSu("chmod 666 " + EVT_FILE);			
		} catch (Exception e) {
			Log.e(TAG, "preparing event file", e);
		}
	}
	
	public int mouseTouchDown(int absX, int absY){
		int ret = intSendEvent(m_fd, EV_REL, KEY_REL_X, absX);
		ret += intSendEvent(m_fd, EV_REL, KEY_REL_Y, absY);
		ret += intSendEvent(m_fd, EV_REL, BTN_TOUCH, 1);
		return ret;
	}

	public int mouseDown(int btn){
		return intSendEvent(m_fd, EV_REL, btn, 1);
	}

	public int mouseUp(int btn){
		return intSendEvent(m_fd, EV_REL, btn, 0);
	}

	public int mouseMove(int relX, int relY){
		int ret = intSendEvent(m_fd, EV_REL, KEY_REL_X, relX);
		ret += intSendEvent(m_fd, EV_REL, KEY_REL_Y, relY);
		return ret;
	}

	public int SendKey(int key, boolean state) {
		if (state)
			return intSendEvent(m_fd, EV_KEY, key, 1); //key down
		else
			return intSendEvent(m_fd, EV_KEY, key, 0); //key up
	}
	
	
							
	private native int		intEnableDebug(int enabled); 	//1 will output to logcat, 0 will disable
	//
	private native int 		intCreate(String dev, int kb, int mouse);
	private native int 		intOpen(String dev);
	private native void		intClose(int fd);
	private native int		intSendEvent(int fd, int type, int code, int value);
	
	static {
		System.loadLibrary("input");
	}*/
}
