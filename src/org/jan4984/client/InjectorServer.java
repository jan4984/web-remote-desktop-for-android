package org.jan4984.client;

import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

import android.content.Context;
import android.hardware.input.IInputManager;
import android.os.IBinder;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class InjectorServer{
	//TODO:power button. for light LCD on. VOLUMN up/down also
	//TODO:drag, for unlock or others.
	final static String TAG = "InjectorServer";

	private static long mDownTime;
	public InjectorServer(){
	}

	public void run(){		
	}

	public static void main(String[] args){
		Log.d(TAG, "injector start...");

		ServerSocket lss = null;
		Socket ls = null;

		try{
			
			lss = new ServerSocket();
			lss.bind(new InetSocketAddress(InetAddress.getByName("127.0.0.1"), 4567));

			while(true){
				InputStreamReader rd;
				char[] buf = new char[512];
				ls = lss.accept();
				rd = new InputStreamReader(ls.getInputStream());
				while(true){
					try{
						int read = rd.read(buf);
						if(read == -1){
							Log.e(TAG, "connection problem");
							break;
						}
						String req = String.copyValueOf(buf, 0, read).trim();
						Log.d(TAG, "received:" + req);
						String[] cmds = req.split(" ");
						LinkedList<String> llcmds = new LinkedList<String>();
						for(String cmd : cmds){
							if(cmd == null || cmd.isEmpty()){
								continue;
							}
							llcmds.add(cmd);
						}
						cmds = llcmds.toArray(new String[]{});
						IInputManager iim = null;
						IBinder bnd = ServiceManager.getService(Context.INPUT_SERVICE);
						if(bnd == null){
							Log.e(TAG, "can not get input service binder, continue read");
							continue;
						}else{
							iim = android.hardware.input.IInputManager.Stub.asInterface(bnd);
						}
						
						for(int i = 0; i < cmds.length; i++){
							String act = cmds[i++];
							int x;
							int y; 
							int keycode;

							long eventTime = SystemClock.uptimeMillis();
							if(act.startsWith("down")){
								mDownTime = eventTime;
							}
							if(act.startsWith("down")){
								x = Integer.valueOf(cmds[i++]);
								y = Integer.valueOf(cmds[i]);
						        MotionEvent event = MotionEvent.obtain(
						        		mDownTime, eventTime, MotionEvent.ACTION_DOWN, x, y, 0);
								event.setSource(InputDevice.SOURCE_TOUCHSCREEN);
								event.setAction(MotionEvent.ACTION_DOWN);								
								iim.injectInputEvent(event, 2);
								event.recycle();
							}else if(act.startsWith("move")){
								x = Integer.valueOf(cmds[i++]);
								y = Integer.valueOf(cmds[i]);
						        MotionEvent event = MotionEvent.obtain(
						        		mDownTime, eventTime, MotionEvent.ACTION_DOWN, x, y, 0);
								event.setSource(InputDevice.SOURCE_TOUCHSCREEN);
								event.setAction(MotionEvent.ACTION_MOVE);								
								iim.injectInputEvent(event, 2);
								event.recycle();
							}else if(act.startsWith("up")){								
								if(eventTime < mDownTime + 20){
									eventTime = mDownTime + 20;
								}
								x = Integer.valueOf(cmds[i++]);
								y = Integer.valueOf(cmds[i]);
						        MotionEvent event = MotionEvent.obtain(
						        		mDownTime, eventTime, MotionEvent.ACTION_DOWN, x, y, 0);
								event.setSource(InputDevice.SOURCE_TOUCHSCREEN);
								event.setAction(MotionEvent.ACTION_UP);
								iim.injectInputEvent(event, 2);
								event.recycle();
							}else if(act.startsWith("key")){
								keycode = Integer.valueOf(cmds[i++]);
								KeyEvent kvt = KeyEvent.obtain(eventTime, eventTime
										, 0/*down*/, keycode, 0, 0
										, -1, 0, 0, InputDevice.SOURCE_KEYBOARD, null);
								iim.injectInputEvent(kvt, 2);
								kvt.recycle();
								kvt = KeyEvent.obtain(eventTime, eventTime + 5
										, 1/*up*/, keycode, 0, 0
										, -1, 0, 0, InputDevice.SOURCE_KEYBOARD, null);
								iim.injectInputEvent(kvt, 2);
								kvt.recycle();
							}														
						}
					}catch(Exception e){
						Log.e(TAG, "processing inject", e);
					}
				}
			}
		}catch(Exception e){
			try{lss.close();}catch(Exception e1){}
			try{ls.close();}catch(Exception e1){}
			Log.e(TAG, "accepting", e);
		}
	}
}
