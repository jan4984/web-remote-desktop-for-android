/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: frameworks/base/core/java/android/hardware/input/IInputManager.aidl
 */
package android.hardware.input;

import android.os.RemoteException;

/** @hide */
public interface IInputManager extends android.os.IInterface {
	/** Local-side IPC implementation stub class. */
	public static abstract class Stub extends android.os.Binder implements
			android.hardware.input.IInputManager {
		private static final java.lang.String DESCRIPTOR = "android.hardware.input.IInputManager";

		/** Construct the stub at attach it to the interface. */
		public Stub() {
			this.attachInterface(this, DESCRIPTOR);
		}
		/**
		 * Cast an IBinder object into an android.hardware.input.IInputManager
		 * interface, generating a proxy if needed.
		 */
		public static android.hardware.input.IInputManager asInterface(
				android.os.IBinder obj) {
			if ((obj == null)) {
				return null;
			}
			android.os.IInterface iin = (android.os.IInterface) obj
					.queryLocalInterface(DESCRIPTOR);
			if (((iin != null) && (iin instanceof android.hardware.input.IInputManager))) {
				return ((android.hardware.input.IInputManager) iin);
			}
			return new android.hardware.input.IInputManager.Stub.Proxy(obj);
		}

		public android.os.IBinder asBinder() {
			return this;
		}

		@Override
		public boolean onTransact(int code, android.os.Parcel data,
				android.os.Parcel reply, int flags)
				throws android.os.RemoteException {
			switch (code) {
			case INTERFACE_TRANSACTION: {
				reply.writeString(DESCRIPTOR);
				return true;
			}
			case TRANSACTION_injectInputEvent: {
				data.enforceInterface(DESCRIPTOR);
				android.view.InputEvent _arg0;
				if ((0 != data.readInt())) {
					_arg0 = android.view.InputEvent.CREATOR
							.createFromParcel(data);
				} else {
					_arg0 = null;
				}
				int _arg1;
				_arg1 = data.readInt();
				boolean _result = this.injectInputEvent(_arg0, _arg1);
				reply.writeNoException();
				reply.writeInt(((_result) ? (1) : (0)));
				return true;
			}
			}
			return super.onTransact(code, data, reply, flags);
		}

		private static class Proxy implements
				android.hardware.input.IInputManager {
			private android.os.IBinder mRemote;

			Proxy(android.os.IBinder remote) {
				mRemote = remote;
			}

			public android.os.IBinder asBinder() {
				return mRemote;
			}

			public java.lang.String getInterfaceDescriptor() {
				return DESCRIPTOR;
			}

			// Injects an input event into the system. To inject into windows
			// owned by other
			// applications, the caller must have the INJECT_EVENTS permission.

			public boolean injectInputEvent(android.view.InputEvent ev, int mode)
					throws android.os.RemoteException {
				android.os.Parcel _data = android.os.Parcel.obtain();
				android.os.Parcel _reply = android.os.Parcel.obtain();
				boolean _result;
				try {
					_data.writeInterfaceToken(DESCRIPTOR);
					if ((ev != null)) {
						_data.writeInt(1);
						ev.writeToParcel(_data, 0);
					} else {
						_data.writeInt(0);
					}
					_data.writeInt(mode);
					mRemote.transact(Stub.TRANSACTION_injectInputEvent, _data,
							_reply, 0);
					_reply.readException();
					_result = (0 != _reply.readInt());
				} finally {
					_reply.recycle();
					_data.recycle();
				}
				return _result;
			}
		}

		static final int TRANSACTION_getInputDevice = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
		static final int TRANSACTION_getInputDeviceIds = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
		static final int TRANSACTION_hasKeys = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
		static final int TRANSACTION_tryPointerSpeed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
		static final int TRANSACTION_injectInputEvent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
		static final int TRANSACTION_getKeyboardLayouts = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
		static final int TRANSACTION_getKeyboardLayout = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
		static final int TRANSACTION_getCurrentKeyboardLayoutForInputDevice = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
		static final int TRANSACTION_setCurrentKeyboardLayoutForInputDevice = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
		static final int TRANSACTION_getKeyboardLayoutsForInputDevice = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
		static final int TRANSACTION_addKeyboardLayoutForInputDevice = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
		static final int TRANSACTION_removeKeyboardLayoutForInputDevice = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
		static final int TRANSACTION_registerInputDevicesChangedListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
		static final int TRANSACTION_vibrate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
		static final int TRANSACTION_cancelVibrate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
	}

	// Injects an input event into the system. To inject into windows owned by
	// other
	// applications, the caller must have the INJECT_EVENTS permission.

	public boolean injectInputEvent(android.view.InputEvent ev, int mode)
			throws android.os.RemoteException;
	// Keyboard layouts configuration.

}
